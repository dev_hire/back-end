<?php

include_once('core/base.php');
try
{
    $controller = new BaseController();
    echo $controller->Output();
}
catch(Exception $ex)
{
    echo Response::json(
        500,
        "Error : ".$ex->getMessage()." occured" 
    );
}
?>