<?php

include_once('../model/libs/engine.php');
include_once('../model/libs/libs.php');
include_once('../view/Response.php');
define('Path', '../');
@ Response::cors();

interface IControllers
{
    function controller_init($route = null);
}

trait TController
{
    final function sub()
    {
        $key = explode('/', $_REQUEST['controller']);
        DBManager::$table = DBManager::$table.strtolower($key[1]);
        unset($key[1]);
        $_REQUEST['controller'] = implode("/", $key);
        return Manager::init();
    }
    
    final function search()
    {
        foreach(DBManager::$key as $index => $values)
        {
            DBManager::$data[$values] = $_POST['search'];
        }
        return Manager::Search();
    }

    final function files($classname = null)
    {
        include_once('../model/FileManager.php');
        FileManager::$filepath = Path.'model/uploads/';
        return FileManager::processFiles(FileManager::$filepath.strtolower($classname), 'file', '');
    }

    final function category()
    {
        $key = explode('/', $_GET['controller']);
        $count = count($key);
        if($count > 2 && $_SERVER['REQUEST_METHOD'] == "GET")
        {   
            return self::controller_init(null);
        }
        DBManager::$table = DBManager::$table.strtolower($key[1]);
        unset($key[1]);
        $_REQUEST['controller'] = $_GET['controller'] = implode("/", $key);
        DBManager::$key[0] = "name";
        return Manager::init();
    }
}

class BaseController extends BasicSecurity
{
    private $status = null, 
            $response,
            $controller,
            $pages = array(404, 400, 500, 403);

    public function __construct() 
    {
        $this->controller = null;
        DataSecurity::init();
        $_REQUEST = parent::logic($_REQUEST);
    }

    public function __destruct()
    {
        $this->controller = null;
        $this->response = null;
        $this->status = null;
    }

    // authorize user function
    private function authUser()
    {
        if($_SERVER['REQUEST_METHOD'] === "GET" 
            && $_REQUEST['controller'] === 'accounts/guest')
        {
            return (string)(!DataSecurity::$headers['x-token'])
                    ? "access denied"
                    : "access granted";
        }
        else{
            return (string)Token::verifyToken(
                DataSecurity::$headers['jwt'], 
                DataSecurity::$headers['x-token']
            );
        }
    }

    private function getControllers()
    {
        $controller = strtolower(explode('/', $_GET['controller'])[0]); 
        return (in_array((int)$controller, $this->pages))
                ? Response::json($controller, Response::$statuscodes[$controller])
                : (file_exists('core/'.strtolower($controller).'.php'))  // Checks if the controller exists
                    ? $this->includeControls($controller)
                    : Response::json(
                        404,
                        "Modules {$controller} not found" 
                    );
    }

    private function includeControls($controls = null)
    {
        try
        {
            require_once('core/'.strtolower($controls).'.php');
            $this->controller = new $controls();
            return $this->controller->controller_init(@explode('/', $_GET['controller'])[1]);
        }
        catch(Exception $ex)
        {
            throw new Exception($ex->getMessage());
        }
    }

    public function Output()
    {
        if($this->authUser() === "access denied")
            return Response::json(
                403,
                "Access denied to server. Authorization required" 
            );
        // check if controller is empty
        return (@ empty(explode('/', $_GET['controller'])[0]) || @ !isset($_GET['controller'])) 
            ? Response::json(
                400,
                "Bad request made to server" 
              )
            : $this->getControllers();
    }
}

?>