<?php

class jobs implements IControllers
{
    use TController;
    private static $jobMap = array('title', 'description', 'duration', 'location', 'expires', 'requirements', 'responsibility', 'level', 'skills', 'quotation', 'jobid', 'type', 'postedby', 'postedon'),
                    $categoryMap = array("name", "summary", "image", "updated", "isparent"),
                    $reviewsMap = array("jobid", "updated", "name", "body"),
                    $ordersMap = array("authid", "refid", "jobname", "jobid", "jobinfo", "status", "name", "date", "updated");
    private $date;

    function __construct()
    {
        $this->date = gmdate('Y-m-d H:i:s');
        include_once('../model/DBM.php');
        Manager::$map = self::$jobMap;
        DBManager::$table = strtolower(__CLASS__);
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $_POST['updated'] = $this->date;
        }
    }

    function __destruct()
    {
        unset($this->date);
    }

    final public function controller_init($func = null)
    {
        switch(strtolower($func))
        {
            case "comments":
            {
                Manager::$map = self::$reviewsMap;
                return $this->sub();
                break;
            }
            case "applicants":
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    $_POST['refid'] = base64_encode($_POST['authid'] . $_POST['name']);
                    DBManager::$key[0] = "refid";
                    $_POST['date'] = ($_POST['date'] == null) ? $this->date : $_POST['date'];
                }
                Manager::$map = self::$ordersMap;
                return $this->sub();
                break;
            }
            case "category":
            {
                Manager::$map = self::$categoryMap;
                return $this->category();
                break;
            }
            case "search":
            {
                DBManager::$key = array("name", "category", "brand", "author", "date", 'price');
                return $this->search();
                break;
            }
            case "files":
            {
                return $this->files(__CLASS__);
                break;
            }
            default: 
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    DBManager::$key[0] = "title";
                    DBManager::$key[1] = "jobid";
                    $_POST['postedon'] = ($_POST['postedon'] == null) ? $this->date : $_POST['postedon'];
                    $_POST['jobid'] = base64_encode($_POST['title']);
                    unset($_POST['updated']);
                }
                return (in_array($func, self::$jobMap) || $func == null)
                        ? Manager::init()
                        : Response::json(400, "Bad request made to service. `{$func}` not found");
                break;
            }
        }
    }
}

?>
