<?php

class posts implements IControllers
{
    use TController;
    private static $postsMap = array("title", "description", "image", "type", "date", "updated", "category", "tags", "author"),
                    $categoryMap = array("name", "summary", "image", "updated", "isparent"),
                    $commentsMap = array("postid", "updated", "name", "body"),
                    $tagMap = array("name", "summary", "updated", "isparent");
    private $date = null;

    function __construct()
    {
        $this->date = gmdate('Y-m-d H:i:s');
        include_once('../model/DBM.php');
        Manager::$map = self::$postsMap;
        DBManager::$table = strtolower(__CLASS__);
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $_POST['updated'] = $this->date;
        }
    }

    function __destruct()
    {
    }

    final public function controller_init($func = null)
    {
        switch(strtolower($func))
        {
            case "comments":
            {
                Manager::$map = self::$commentsMap;
                return $this->sub();
                break;
            }
            case "tags":
            {
                Manager::$map = self::$tagMap;
                return $this->sub();
                break;
            }
            case "category":
            {
                Manager::$map = self::$categoryMap;
                return $this->category();
                break;
            }
            case "search":
            {
                DBManager::$key = array("title", "category", "tags", "author", "date");
                return $this->search();
                break;
            }
            case "files":
            {
                return $this->files(__CLASS__);
                break;
            }
            default: 
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    DBManager::$key[0] = "title";
                    DBManager::$key[1] = "author";
                    $_POST['date'] = ($_POST['date'] == null) ? $this->date : $_POST['date'];
                }
                return (in_array($func, self::$postsMap) || $func == null)
                        ? Manager::init()
                        : Response::json(400, "Bad request made to posts. `{$func}` not found");
                break;
            }
        }
    }
}

?>