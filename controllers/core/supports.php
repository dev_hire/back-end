<?php

class supports implements IControllers
{
    use TController;
    private static $faqMap = array("question", "answer", "updated"),
                    $testimonialsMap = array("name", "updated", 'image', "body"),
                    $slidesMap = array("name", "details", "updated");

    private $date = null;

    function __construct()
    {
        $this->date = gmdate('Y-m-d H:i:s');
        include_once('../model/DBM.php');
        DBManager::$table = strtolower(__CLASS__);
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $_POST['updated'] = $this->date;
        }
    }

    function __destruct()
    {
    }

    final public function controller_init($func = null)
    {
        switch(strtolower($func))
        {
            case "faq":
            {
                Manager::$map = self::$faqMap;
                return $this->sub();
                break;
            }
            case "testimonials":
            {
                Manager::$map = self::$testimonialsMap;
                return $this->sub();
                break;
            }
            case "slides":
            {
                Manager::$map = self::$slidesMap;
                return $this->sub();
                break;
            }
            default: 
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    $_POST['date'] = ($_POST['date'] == null) ? $this->date : $_POST['date'];
                }
                return (in_array($func, self::$supportMap) || $func == null)
                        ? Manager::init()
                        : Response::json(400, "Bad request made to posts. `{$func}` not found");
                break;
            }
        }
    }
}

?>