<?php

class email implements IControllers
{
    private static $subscribersMap = array("name", "email", "region", "updated"),
                    $messagesMap = array('title', 'subject', 'message', 'created', 'by', 'updated'),
                    $response, $resp = array(), $data;
    private $date;

    function __construct()
    {
        include_once('../model/DBM.php');
        include_once('../model/Mailing.php');
        $this->date = gmdate('Y-m-d H:i:s');
    }

    function __destruct()
    {
    }

    final public function controller_init($func = null)
    {
        TMail::init();
        switch(strtolower($func))
        {
            case "contact":
            {
                return $this->contact();
                break;
            }
            case "subscribers":
            {
                return $this->subscribers();
                break;
            }
            case "newsletter":
            {
                return $this->Newsletter();
                break;
            }
            case "messages":
            {
                return $this->newsletterMessages();
                break;
            }
            default:
            {
                return Response::json(400, "Bad request parameter not found");
                break;
            }
        }
    }
    
    private function contact()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST')
            return Response::json(405, 'Request Method not allowed for function');
            
        TMail::$mailler->From = $_POST['email'];
        TMail::$mailler->FromName = $_POST['sender'];
        TMail::$mailler->addReplyTo($_POST['email']);
        TMail::$mailler->addAddress(TMail::$mailConfig['From']);
        TMail::$mailler->Subject = $_POST['subject'];
        TMail::$mailler->Body = $_POST['message'];
        return TMail::Incoming();
    }
    
    public function notify()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST')
            return Response::json(405, 'Request Method not allowed for function');

        TMail::$mailler->From = $_POST['email'];
        TMail::$mailler->FromName = $_POST['sender'];
        TMail::$mailler->addReplyTo($_POST['email']);
        TMail::$mailler->addAddress($_POST['recipent']);
        TMail::$mailler->Subject = $_POST['subject'];
        TMail::$mailler->Body = $_POST['message'];
        return TMail::Incoming();
    }
    
    private function subscribers()
    {
        $key = explode('/', $_REQUEST['controller']);
        DBManager::$table = DBManager::$table.strtolower('subscribers');
        unset($key[1]);
        $_REQUEST['controller'] = implode("/", $key);
        Manager::$map = self::$subscribersMap;
        if($_SERVER['REQUEST_METHOD'] == "POST")
        {
            DBManager::$key[0] = 'mail';
            $_POST['updated'] = $this->date;
        }
        return Manager::init();
    }
    
    private function newsletterMessages()
    {
        $key = explode('/', $_REQUEST['controller']);
        DBManager::$table = strtolower('newslettermessages');
        unset($key[1]);
        $_REQUEST['controller'] = implode("/", $key);
        Manager::$map = self::$messagesMap;
        if($_SERVER['REQUEST_METHOD'] == "POST")
        {
            DBManager::$key[0] = 'title';
            DBManager::$key[1] = 'subject';
            $_POST['created'] = $this->date;
            $_POST['updated'] = $this->date;
        }
        return Manager::init();
    }

    private function sendNewsletter()
    {
        $key = explode('/', $_REQUEST['controller']);
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $subscribers = json_decode($this->subscribers(), TRUE)['response'];
        $messages = json_decode($this->newsletterMessages(), TRUE)['response'];
        // DBManager::connect();
        // self::$response = DBManager::Read();
        // self::$data['message'] = $_POST['message'];
        // self::$data['subject'] = $_POST['subject'];
        // $_SERVER['REQUEST_METHOD'] = 'POST';
        // foreach(self::$response as $key => $subscribers)
        // {
        //     TMail::$mailler->addAddress(TMail::$mailConfig['From']);
        //     self::$data['email'] = $subscribers['mail'];
        //     self::$data['sender'] = $subscribers['name'];
        //     self::$resp[$subscribers['name']] = str_replace("Message", "Newsletter", TMail::Outgoing(self::$data));
        // }
        // DBManager::disconnect();
        // return Response::json(200, self::$resp);
    }

    private function Newsletter()
    {
        switch(strtolower(explode('/', $_GET['controller'])[2]))
        {
            case "messages":
            {
                DBManager::$table = strtolower("newslettermsgs");
                $_POST['created'] = Date('Y:m:d');
                Manager::$map = array('title', 'subject', 'message', 'created', 'by');
                DBManager::$key[0] = ($_SERVER['REQUEST_METHOD']) ? 'id' : 'title';
                $key = explode('/', $_REQUEST['controller']);
                unset($key[1]);
                unset($key[2]);
                $_REQUEST['controller'] = implode("/", $key);
                return Manager::init();
                break;
            }
            case "":
            {
                return self::sendNewsletter();
                break;
            }
            default:
            {
                return Response::json(400, "Bad newsletter request made to server");
                break;
            }
        }
    }

}

?>