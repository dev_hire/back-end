<?php

class Accounts implements IControllers
{
    private static $acctMap = array("username", "firstname", "lastname", "middlename", "sex", 'status', "email", "mobile", "address", 'authid', 'access'),
                    $authMap = array("username", "email", "password", "authid");
    private $email;

    function __construct()
    {
        include_once('../model/DBM.php');
        include_once('../model/FileManager.php');
        Manager::$map = self::$acctMap;
        DBManager::$table = strtolower(__CLASS__);
    }

    function __destruct()
    {
    }

    final public function controller_init($func = null)
    {
        switch(strtolower($func))
        {
            case "login":
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    DBManager::$key[0] = "authid";
                    DBManager::$key[1] = "username";
                    $_POST['authid'] = base64_encode($_POST['username']);
                }
                return $this->login();
                break;
            }
            case "register":
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    DBManager::$key[0] = "authid";
                    DBManager::$key[1] = "username";
                    $_POST['authid'] = base64_encode($_POST['username']);
                }
                return $this->register();
                break;
            }
            case "forgot":
            {
                return $this->forgot();
                break;
            }
            case "auth":
            {
                if($_SERVER['REQUEST_METHOD'] !== 'GET')
                    return Response::json(405, "Request method not supportted by request made. Please change your request method");
                
                $route = explode('/', $_GET['controller']);
                if(!isset($route[2]))
                    return Response::json(400, "Missing authid parameter");
                    
                DBManager::$key[0] = "authid";
                $data = AuthModel::getAuthData(array(
                    "authid" => $route[2],
                ));
                return (is_string($data))
                    ? Response::json('400', 'User does not exist. Please register')
                    : Response::json(200, $data);
                break;
            }
            case "remove":
            {
                return $this->remove();
                break;
            }
            case "guest":
            {
                return $this->guest();
                break;
            }
            default: 
            {
                if($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    DBManager::$key[0] = "authid";
                    DBManager::$key[1] = "username";
                }
                return (in_array($func, self::$acctMap) || $func == null)
                        ? Manager::init()
                        : Response::json(400, "Bad request made to accounts. `{$func}` not found");
                break;
            }
        }
    }

    final private function login()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        $data = AuthModel::getAuthData($_POST);
        return (is_string($data))
                ? Response::json('400', 'User does not exist. Please register')
                : AuthModel::validate($_POST['password'], $data, SALT);
    }

    final private function register()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        DBManager::$table = strtolower(DBManager::$table."auth");
        $_POST['password'] = _create_hash($_POST['password'], SALT);
        $_REQUEST['controller'] = $_GET['controller'] = 'accounts';
        foreach(DBManager::$key as $index => $values)
        {
            $_GET['controller'] .= '/'.$values.'/'.$arr[$values];
        }
        $_REQUEST['controller'] = $_GET['controller'];
        Manager::$map = self::$authMap;
        $_SERVER['REQUEST_METHOD'] = "POST";
        return Manager::init();

        // DBManager::$table = strtolower(__CLASS__);
        // $_SERVER['REQUEST_METHOD'] = "GET";
        // $_REQUEST['controller'] = $_GET['controller'] = "accounts/username/".$_POST['username'];
        // $user = json_decode(Manager::init(), true)['response'][0];
        // $this->sendEmail($response, $user);
    }

    // private function sendEmail($response = array(), $userInfo)
    // {
    //     include_once('../controllers/core/email.php');
    //     $emailconfig = parse_ini_file(Path."model/config/mail.conf");
    //     $this->email = new Email();
    //     $_POST['sender'] = $emailconfig['FromName'];
    //     $_POST['email'] = $emailconfig['From'];
    //     $_POST['recipent'] = $userInfo['email'];
    //     $_POST['subject'] = "Register email";
    //     $message = read_file(Path . "view/emails/register.html");
    //     $message = str_replace("{{username}}", $userInfo['username'], $message);
    //     echo $message;


    //     $_SERVER['REQUEST_METHOD'] = "POST";
    //     // echo $this->email->notify();
    // }

    final private function guest()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'GET')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");

        return Response::json(200, 
                        array(
                            "token" => Token::generateToken("guest", DataSecurity::$headers['x-token']), 
                            "data" => array(
                                "username" => "John Doe",
                            ),
                        )
                    );
    }
    
    final private function forgot()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'PUT')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        parse_str(file_get_contents("php://input"), $GLOBALS['PUT']);
        $GLOBALS['PUT']['authid'] = base64_encode($GLOBALS['PUT']['username']);
        DBManager::$key = array("username", "authid");
        $data = AuthModel::getAuthData($GLOBALS['PUT']);
        if($data == "not found");
            return Response::json(400, "User do not exists");
        
        foreach($data as $index => $values)
        {
            $GLOBALS['PUT'][$index] = ($index != "password")
                                        ? $values
                                        : _create_hash($GLOBALS['PUT']['password'], SALT);
        }
        $_REQUEST['controller'] = $_GET['controller'] = "accounts/username/" . $GLOBALS['PUT']['username'];
        Manager::$map = self::$authMap;
        $_SERVER['REQUEST_METHOD'] = "PUT";
        return Manager::init();
    }
    
    final private function remove()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'DELETE')
            return Response::json(405, "Request method not supportted by request made. Please change your request method");
            
        DBManager::$table = strtolower(DBManager::$table."auth");
        $key = explode('/', $_GET['controller']);
        if(count($key) < 2 || count($key) < 3 || count($key) < 4)
            return Response::json(400, "Invalid request made to server.");

        unset($key[1]);
        $_REQUEST['controller'] = $_GET['controller'] = implode('/', $key);
        return Manager::init();
    }
}

?>