<?php

class api
{
    private static $url = "almondcareerstestapi.azurewebsites.net/api/";

    function getApiData($controller = null)
    {
        $response = json_decode(
                curl::sendRequest(
                    null, 
                    null, 
                    self::$url.$controller, 
                    [], 
                    "GET"
                ),
                true
            );
        return (!isset($response))
                    ? $response
                    : $response;
    }
}


?>