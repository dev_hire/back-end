<?php
// Database Libraries
require_once('DB/rb.php');
require_once('DB/jsonDB.php');
require_once('DB/DBManager.php');
require_once('DB/JSONManager.php');
// Security Libraries
require_once("Security/secure/firewall.php");
require_once("Security/secure/rfi.php");
require_once("Security/secure/xss_clean.php");
require_once("Security/cryptography/SecureData.php");
require_once("Security/cryptography/LibCrypt.php");
require_once("Security/cryptography/Crypt.php");
require_once("Security/authentication/token.php");
require_once("Security/authentication/VerifyEmail.php");
require_once('Security/security.php');
// Uploader Counter Library
require_once('Uploader/BulletProof.php');
// Uploader Counter Library
require_once('Curl/curl.php');
// Email
require_once('Mailer/PHPMailerAutoload.php');
// Firebase Library
require_once('firebase/firebaseLib.php');
require_once('firebase/firebaseInterface.php');
require_once('firebase/firebaseStub.php');


const DB_TYPE = 'nosql';


?>