<?php

class Token extends DataSecurity
{
    public static function generateToken($access = 'guest', $key = null)
    {
        // $csrf = base64_encode(bin2hex(openssl_random_pseudo_bytes(12))).$key;
        return base64_encode(parent::cryptoJsAesEncrypt(
                $key, 
                array(
                    "realm" => $_SERVER['HTTP_HOST'],
                    "ipaddress" => $_SERVER['REMOTE_ADDR'],
                    "browser" => @ $_SERVER['HTTP_USER_AGENT'],
                    "access" => $access,
                    "csrf" => $key,
                    "issued" => date('d:m:Y'),
                    "expires" => date('d:m:Y', strtotime('+5 days')),
                )
            )        
        );
    }

    private static function authenticateToken($data = array(), $csrf = null)
    {
        //print_r($data);
        return ($_SERVER['REMOTE_ADDR'] === $data['ipaddress'] 
                && @ $_SERVER['HTTP_USER_AGENT'] === $data['browser'] 
                && $csrf === $data['csrf'])
                    ? "access granted"
                    : "access denied";
    }

    public static function verifyToken($jwt = null, $key = null)
    {
        return self::authenticateToken(parent::cryptoJsAesDecrypt($key, base64_decode($jwt)), $key);
    }
}

?>